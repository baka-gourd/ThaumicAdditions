#Project info
author=Zeitheron
start_git_commit=c417f81fc2487b8e583569fb2c9a60ec675a270b
git_commit=https://gitlab.com/DragonForge/ThaumicAdditions/commit/{{hash}}

#Runtime info
forge_version=14.23.5.2847
minecraft_version=1.12.2
mcp=stable_39
hl_version=2.0.6.17
jei_version=4.11.0.206

#Mod info
trusted_build=false
mod_id=thaumadds
mod_id_fancy=ThaumicAdditions
mod_name=Thaumic Additions: Reconstructed
mod_version=12.4.3
cf_project=232564
release_type=release